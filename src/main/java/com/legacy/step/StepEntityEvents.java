package com.legacy.step;

import com.legacy.step.player.StepPlayer;
import com.legacy.step.player.util.CapabilityProvider;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class StepEntityEvents
{
	@SubscribeEvent
	public void onCapabilityAttached(AttachCapabilitiesEvent<Entity> event)
	{
		if (event.getObject() instanceof PlayerEntity && !event.getObject().getCapability(StepPlayer.INSTANCE).isPresent())
			event.addCapability(StepMod.locate("player_capability"), new CapabilityProvider(new StepPlayer((PlayerEntity) event.getObject())));
	}

	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof PlayerEntity)
			StepPlayer.ifPresent((PlayerEntity) event.getEntity(), stepPlayer -> stepPlayer.tick());
	}
}
