package com.legacy.step.player.util;

import com.legacy.step.player.StepPlayer;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

public class CapabilityProvider implements ICapabilitySerializable<CompoundNBT>
{
	private final LazyOptional<IStepPlayer> playerHandler;

	public CapabilityProvider(IStepPlayer skyPlayer)
	{
		this.playerHandler = LazyOptional.of(() -> skyPlayer);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		return cap == StepPlayer.INSTANCE ? this.playerHandler.cast() : LazyOptional.empty();
	}

	@Override
	public void deserializeNBT(CompoundNBT compound)
	{
		this.playerHandler.orElse(null).read(compound);
	}

	@Override
	public CompoundNBT serializeNBT()
	{
		CompoundNBT compound = new CompoundNBT();
		this.playerHandler.orElse(null).writeAdditional(compound);
		return compound;
	}
}