package com.legacy.step.player.util;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class CapabilityStorage implements IStorage<IStepPlayer>
{
	@Override
	public INBT writeNBT(Capability<IStepPlayer> capability, IStepPlayer instance, Direction side)
	{
		CompoundNBT compound = new CompoundNBT();
		instance.writeAdditional(compound);
		return compound;
	}

	@Override
	public void readNBT(Capability<IStepPlayer> capability, IStepPlayer instance, Direction side, INBT nbt)
	{
		CompoundNBT compound = (CompoundNBT) nbt;
		instance.read(compound);
	}
}