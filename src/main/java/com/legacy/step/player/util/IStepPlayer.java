package com.legacy.step.player.util;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;

public interface IStepPlayer
{
	CompoundNBT writeAdditional(CompoundNBT nbt);

	void read(CompoundNBT nbt);

	boolean hasResetStepHeight();

	void resetStepHeight(boolean updated);

	PlayerEntity getPlayer();
	
	void tick();
}