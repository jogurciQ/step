package com.legacy.step.player;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.step.StepRegistry;
import com.legacy.step.player.util.IStepPlayer;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.util.NonNullSupplier;

public class StepPlayer implements IStepPlayer
{
	@CapabilityInject(IStepPlayer.class)
	public static Capability<IStepPlayer> INSTANCE = null;

	private PlayerEntity player;
	private boolean hasResetStepHeight = true;

	public StepPlayer()
	{
	}

	public StepPlayer(PlayerEntity player)
	{
		super();
		this.player = player;
	}

	@Nullable
	public static IStepPlayer get(PlayerEntity player)
	{
		return StepPlayer.getIfPresent(player, (skyPlayer) -> skyPlayer);
	}

	public static <E extends PlayerEntity> void ifPresent(E player, Consumer<IStepPlayer> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			action.accept(player.getCapability(INSTANCE).resolve().get());
	}

	@Nullable
	public static <E extends PlayerEntity, R> R getIfPresent(E player, Function<IStepPlayer, R> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return null;
	}

	public static <E extends PlayerEntity, R> R getIfPresent(E player, Function<IStepPlayer, R> action, NonNullSupplier<R> elseSupplier)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return elseSupplier.get();
	}

	@Override
	public CompoundNBT writeAdditional(CompoundNBT compound)
	{
		compound.putBoolean("ResetStepEnchHeight", this.hasResetStepHeight());

		return compound;
	}

	@Override
	public void read(CompoundNBT compound)
	{
		this.resetStepHeight(compound.getBoolean("ResetStepEnchHeight"));
	}

	@Override
	public PlayerEntity getPlayer()
	{
		return this.player;
	}

	@Override
	public boolean hasResetStepHeight()
	{
		return this.hasResetStepHeight;
	}

	@Override
	public void resetStepHeight(boolean update)
	{
		this.hasResetStepHeight = update;
	}

	@Override
	public void tick()
	{
		PlayerEntity entity = this.getPlayer();

		float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(StepRegistry.STEPPING, entity);
		float defaultStepHeight = 0.6F;

		if (enchantmentLevel > 0 && entity.stepHeight < enchantmentLevel)
		{
			entity.stepHeight = enchantmentLevel + 0.1F;

			this.resetStepHeight(false);
		}
		else if (entity.stepHeight > defaultStepHeight && enchantmentLevel <= 0)
		{
			if (!hasResetStepHeight)
			{
				entity.stepHeight = defaultStepHeight;
				this.resetStepHeight(true);
			}
		}
	}
}
