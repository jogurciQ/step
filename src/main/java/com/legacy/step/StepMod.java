package com.legacy.step;

import com.legacy.step.player.StepPlayer;
import com.legacy.step.player.util.CapabilityStorage;
import com.legacy.step.player.util.IStepPlayer;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(StepMod.MODID)
public class StepMod
{
	public static final String NAME = "Step";
	public static final String MODID = "step";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public StepMod()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(StepMod::commonInit);
		MinecraftForge.EVENT_BUS.register(new StepEntityEvents());
	}

	public static void commonInit(final FMLCommonSetupEvent event)
	{
		CapabilityManager.INSTANCE.register(IStepPlayer.class, new CapabilityStorage(), StepPlayer::new);
	}
}